import supertest from 'supertest';
import app from '@app/server';
import { BaseTransaction } from '@app/models/Transaction';

const request = supertest.agent(app);

describe(`Calling POST /transactions with an invalid body`, function () {
    it(`should return 400 response`, async function () {
        const { status } = await request.post(`/transactions`);

        expect(status).toEqual(400);
    });
});

describe(`Calling POST /transactions with a valid body`, function () {
    it(`should create a new transaction (201 response)`, async function () {
        const { body: customers } = await request.get(`/customers`);
        const [customer] = customers;
        const payload: BaseTransaction = {
            amount: 12300,
            customerId: customer.id,
            split: 3,
            storeName: 'Leroy Merlin',
        };
        expect.assertions(3);
        try {
            const { body, status } = await request
                .post(`/transactions`)
                .send(payload);

            expect(status).toEqual(201);
            expect(body).toEqual(
                expect.objectContaining({
                    amount: 12300,
                    customerId: customer.id,
                    split: 3,
                    storeName: 'Leroy Merlin',
                    isCompleted: false,
                })
            );
            expect(body).toHaveProperty('createdDate');
        } catch (error) {
            console.error(error);
        }
    });
});
