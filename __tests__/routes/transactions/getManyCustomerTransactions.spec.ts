import supertest from 'supertest';
import app from '@app/server';

const request = supertest.agent(app);

describe(`Calling GET /customers/:id/transactions with a non-existing customer`, function () {
    it(`should return the customer's transactions`, async function () {
        const { status } = await request.get(`/customers/0/transactions`);

        expect(status).toEqual(404);
    });
});

describe(`Calling GET /customers/:id/transactions with an existing customer`, function () {
    it(`should return the customer's transactions`, async function () {
        const { body: customers } = await request.get(`/customers`);
        const [customer] = customers;
        const { body: transactions, status } = await request.get(
            `/customers/${customer.id}/transactions`
        );

        expect(status).toEqual(200);
        expect(transactions.length).toBeGreaterThan(0);
        const [transaction] = transactions;
        expect(transaction).toHaveProperty('customerId');
        expect(transaction).toHaveProperty('amount');
        expect(transaction).toHaveProperty('isCompleted');
        expect(transaction).toHaveProperty('storeName');
        expect(transaction).toHaveProperty('split');
        expect(transaction).toEqual(
            expect.objectContaining({ customerId: customer.id })
        );
    });
});
