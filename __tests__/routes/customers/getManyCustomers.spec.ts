import supertest from 'supertest';
import app from '@app/server';

const request = supertest.agent(app);

describe(`Calling GET /customers`, function () {
    it(`should return an array of customers`, async function () {
        const { body, status } = await request.get(`/customers`);

        expect(status).toEqual(200);
        expect(body).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    name: 'Jon DO',
                }),
                expect.objectContaining({
                    name: 'Jane DO',
                }),
            ])
        );
    });
});
