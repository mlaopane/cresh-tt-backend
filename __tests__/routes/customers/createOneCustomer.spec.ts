import supertest from 'supertest';
import app from '@app/server';

const request = supertest.agent(app);

describe(`Calling POST /customers with an invalid body`, function () {
    it(`should return 400 response`, async function () {
        const { status } = await request.post(`/customers`);

        expect(status).toEqual(400);
    });
});

describe(`Calling POST /customers with an valid body`, function () {
    it(`should create a new customer and a 201 response`, async function () {
        const beforeCreationDate = new Date();

        const { body, status } = await request
            .post(`/customers`)
            .send({ name: 'Richard' });

        expect(status).toEqual(201);
        expect(body).toEqual(expect.objectContaining({ name: 'Richard' }));
        expect(body).toHaveProperty('createdDate');
        const createdDate = new Date(body['createdDate']);
        expect(createdDate.getTime()).toBeGreaterThanOrEqual(
            beforeCreationDate.getTime()
        );
        expect(createdDate.getTime()).toBeLessThanOrEqual(
            new Date().getTime()
        );
    });
});
