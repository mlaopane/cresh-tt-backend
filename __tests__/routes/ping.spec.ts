import supertest from 'supertest';
import app from '@app/server';

const request = supertest.agent(app);

describe(`Calling GET /ping to test the server and the database`, function () {
    it(`should return a 200 response`, async function () {
        expect.assertions(1);
        const { status } = await request.get(`/ping`);
        expect(status).toEqual(200);
    });
});
