import supertest from 'supertest';
import app from '@app/server';
import { BaseTransaction } from '@app/models/Transaction';

const request = supertest.agent(app);

describe(`Calling GET /transactions/:id/instalments with a non-existing transaction`, function () {
    it(`should return the a 404 response`, async function () {
        const { status } = await request.get(`/transactions/0/instalments`);

        expect(status).toEqual(404);
    });
});

describe(`Calling GET /transactions/:id/instalments with an existing transaction`, function () {
    it(`should return the transaction's instalments`, async function () {
        const { body: customers } = await request.get(`/customers`);
        const [customer] = customers;

        const newTransaction: BaseTransaction = {
            amount: 5000,
            customerId: customer.id,
            split: 3,
            storeName: 'Castorama',
        };

        const { body: createdTransaction } = await request
            .post(`/transactions`)
            .send(newTransaction);

        const { body, status } = await request.get(
            `/transactions/${createdTransaction.id}/instalments`
        );

        expect(status).toEqual(200);
        expect(body).toHaveLength(3);
        expect(body).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    amount: 1668,
                    isPaid: true,
                    transactionId: createdTransaction.id,
                }),
                expect.objectContaining({
                    amount: 1666,
                    isPaid: false,
                    transactionId: createdTransaction.id,
                }),
            ])
        );
    });
});
