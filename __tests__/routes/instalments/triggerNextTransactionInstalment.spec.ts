import supertest from 'supertest';
import app from '@app/server';
import { BaseTransaction } from '@app/models/Transaction';

const request = supertest.agent(app);

describe(`Calling PUT /transactions/:id/instalments with a non-existing transaction`, function () {
    it(`should return the a 404 response`, async function () {
        const { status } = await request.put(`/transactions/0/instalments`);

        expect(status).toEqual(404);
    });
});

describe(`Calling PUT /transactions/:id/instalments with an existing transaction`, function () {
    it(`should trigger the payment for the next transaction's instalment`, async function () {
        const { body: customers } = await request.get(`/customers`);
        const [customer] = customers;
        const transactionInput: BaseTransaction = {
            amount: 12300,
            customerId: customer.id,
            split: 3,
            storeName: 'Leroy Merlin',
        };

        const { body: transaction } = await request
            .post(`/transactions`)
            .send(transactionInput);

        const { status } = await request.put(
            `/transactions/${transaction.id}/instalments`
        );

        expect(status).toEqual(204);

        const { body: instalments } = await request.get(
            `/transactions/${transaction.id}/instalments`
        );

        expect(
            instalments.filter((instalment) => instalment.isPaid)
        ).toHaveLength(2);
    });

    it(`should return a 409 response when the transaction is completed`, async function () {
        const { body: customers } = await request.get(`/customers`);
        const [customer] = customers;
        const newTransaction: BaseTransaction = {
            amount: 5000,
            customerId: customer.id,
            split: 3,
            storeName: 'Castorama',
        };

        const { body: createdTransaction } = await request
            .post(`/transactions`)
            .send(newTransaction);

        await request.put(
            `/transactions/${createdTransaction.id}/instalments`
        );

        await request.put(
            `/transactions/${createdTransaction.id}/instalments`
        );

        const { status } = await request.put(
            `/transactions/${createdTransaction.id}/instalments`
        );

        expect(status).toEqual(409);

        const { body: customerTransactions } = await request.get(
            `/customers/${customer.id}/transactions`
        );

        expect(
            customerTransactions.filter(
                (transaction) => transaction.isCompleted
            )
        ).toHaveLength(1);

        const { body: instalments } = await request.get(
            `/transactions/${createdTransaction.id}/instalments`
        );

        expect(
            instalments.filter((instalment) => instalment.isPaid)
        ).toHaveLength(3);
    });
});
