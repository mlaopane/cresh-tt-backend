NETWORK=cresh

clean: migrations_down db_stop app_stop network_remove
init: packages_install network_create db_start migrations_up db_initialize app_start

start: db_start
stop: db_stop

app_start:
	docker-compose --file docker-compose.yaml up --detach
app_stop:
	docker-compose --file docker-compose.yaml down

db_initialize:
	yarn db:initialize
db_start:
	docker-compose --file db/docker-compose.yaml up --detach
db_stop:
	docker-compose --file db/docker-compose.yaml down

migrations_down:
	docker-compose --file db/docker-compose.yaml run --rm cresh.database.down
migrations_up:
	docker-compose --file db/docker-compose.yaml run --rm cresh.database.up || true

network_create:
	docker network create $(NETWORK) --driver bridge || true
network_remove:
	docker network rm $(NETWORK) || true

packages_install:
	yarn install
