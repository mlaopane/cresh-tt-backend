module.exports = {
    apps: [
        {
            name: 'app',
            script: 'ts-node',
            args: '-r tsconfig-paths/register src/start.ts',
            env: {
                NODE_ENV: process.env.NODE_ENV,
                DATABASE_NAME: process.env.DATABASE_NAME,
                SERVER_PORT: process.env.SERVER_PORT,
            },
            watch: ['src'],
        },
    ],
};
