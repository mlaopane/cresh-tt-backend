import pool from '../../src/database';
import { PoolClient } from 'pg';

async function seedCustomers(client: PoolClient) {
    try {
        await client.query('DELETE FROM customers');
        const result = await client.query(
            `INSERT INTO customers (name, "createdDate")
            VALUES
                ('Jane DO', NOW()),
                ('Jon DO', NOW())
            RETURNING *;`
        );

        return result.rows;
    } catch (error) {
        console.error(error);
    }
}

export async function initializeCustomers() {
    const client = await pool.connect();
    const customers = await seedCustomers(client);
    client.release();

    return customers;
}
