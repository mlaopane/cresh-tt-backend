import { initializeCustomers } from './initializeCustomers';
import { initializeTransactions } from './initializeTransactions';

export async function initializeDatabase() {
    const customers = await initializeCustomers();
    await initializeTransactions(customers);
}

initializeDatabase()
    .then(() => {
        console.log(`Database intialized`);
    })
    .catch((error) => {
        console.error(`The database cannot be initialized`, error);
    });
