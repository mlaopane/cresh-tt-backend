import pool from '../../src/database';
import { PoolClient } from 'pg';

async function seedTransactions(client: PoolClient, customers) {
    try {
        await client.query('DELETE FROM transactions');
        const result = await client.query(
            `INSERT INTO transactions (amount, "createdDate", "customerId", "isCompleted", split, "storeName")
            VALUES ($1, $2, $3, $4, $5, $6)
            RETURNING *;`,
            [5000, new Date(), customers[0].id, false, 3, 'Boulanger']
        );

        return result.rows;
    } catch (error) {
        console.error(error);
    }
}

export async function initializeTransactions(customers) {
    const client = await pool.connect();
    const transactions = await seedTransactions(client, customers);
    client.release();

    return transactions;
}
