import pool from '../../src/database';

export function shutdownDatabase() {
    return pool.end();
}

shutdownDatabase().then(() => {
    console.log(`Database shutdown`);
});
