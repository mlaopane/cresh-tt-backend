'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = async function (db) {
    await db.createTable('customers', {
        columns: {
            id: { type: 'int', primaryKey: true, autoIncrement: true },
            name: { type: 'text', notNull: true },
            createdDate: { type: 'date', notNull: true },
        },
        ifNotExists: true,
    });
    await db.createTable('transactions', {
        columns: {
            id: { type: 'int', primaryKey: true, autoIncrement: true },
            createdDate: { type: 'date', notNull: true },
            customerId: {
                type: 'int',
                foreignKey: {
                    name: 'transaction_customer_fk',
                    table: 'customers',
                    rules: {
                        onDelete: 'CASCADE',
                        onUpdate: 'CASCADE',
                    },
                    mapping: 'id',
                },
                notNull: true,
            },
            isCompleted: { type: 'boolean', notNull: true },
            amount: { type: 'int', notNull: true, desription: 'in cents' },
            storeName: { type: 'text', notNull: true },
            split: { type: 'int', notNull: true },
        },
        ifNotExists: true,
    });
    await db.createTable('instalments', {
        columns: {
            id: { type: 'int', primaryKey: true, autoIncrement: true },
            transactionId: {
                type: 'int',
                foreignKey: {
                    name: 'instalment_transaction_fk',
                    table: 'transactions',
                    rules: {
                        onDelete: 'CASCADE',
                        onUpdate: 'CASCADE',
                    },
                    mapping: 'id',
                },
                notNull: true,
            },
            amount: { type: 'int', notNull: true, desription: 'in cents' },
            isPaid: { type: 'boolean', notNull: true },
            paidDate: { type: 'date', notNull: false },
            plannedDate: { type: 'date', notNull: true },
        },
        ifNotExists: true,
    });
};

exports.down = async function (db) {
    await db.dropTable('instalments', {
        ifExists: true,
    });
    await db.dropTable('transactions', {
        ifExists: true,
    });
    await db.dropTable('customers', {
        ifExists: true,
    });
};

exports._meta = {
    version: 1,
};
