module.exports = {
    clearMocks: true,
    moduleDirectories: ['src', 'node_modules'],
    moduleFileExtensions: ['js', 'json', 'ts'],
    moduleNameMapper: {
        '@root/(.*)$': '<rootDir>/$1',
        '@app/(.*)$': '<rootDir>/src/$1',
    },
    preset: 'ts-jest',
    testEnvironment: 'node',
    testMatch: ['**/__tests__/**/*.ts', '**/?(*.)+spec.ts'],
};
