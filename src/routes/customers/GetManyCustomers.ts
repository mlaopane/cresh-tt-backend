import { Request, Response } from 'express';
import { Route, Method } from '@mlaopane/easy-router';
import CustomerRepository from '@app/services/CustomerRepository';

async function getManyCustomers(request: Request, response: Response) {
    const customers = await CustomerRepository.getManyCustomers();
    response.status(200).json(customers);
}

const GetManyCustomers: Route = {
    handle: getManyCustomers,
    method: Method.Get,
    middlewares: [],
    path: '/customers',
};

export default GetManyCustomers;
