import { Request, Response } from 'express';
import { Route, Method } from '@mlaopane/easy-router';
import CustomerRepository from '@app/services/CustomerRepository';
import CustomerFactory from '@app/services/CustomerFactory';
import Joi from '@hapi/joi';
import BadRequestHttpError from '@app/errors/BadRequestHttpError';

const schema = Joi.object({
    name: Joi.string().required(),
});

async function validateRequest(request: Request) {
    try {
        await schema.validateAsync(request.body);
    } catch (error) {
        throw new BadRequestHttpError({ errors: error.details });
    }
}

async function createOneCustomer(request: Request, response: Response) {
    await validateRequest(request);

    const customer = CustomerFactory.createCustomerFromInput(request.body);

    const persistedCustomer = await CustomerRepository.insertOneCustomer(
        customer
    );

    response.status(201).json(persistedCustomer);
}

const CreateOneCustomer: Route = {
    handle: createOneCustomer,
    method: Method.Post,
    middlewares: [],
    path: '/customers',
};

export default CreateOneCustomer;
