import CreateOneCustomer from './CreateOneCustomer';
import GetManyCustomers from './GetManyCustomers';

export default { CreateOneCustomer, GetManyCustomers };
