import { Request, Response } from 'express';
import { Route, Method } from '@mlaopane/easy-router';
import db from '@app/database';

async function ping(request: Request, response: Response) {
    try {
        const client = await db.connect();
        client.release();
        await db.end();
        response.status(200).end();
    } catch (error) {
        response.status(500).end();
    }
}

const Ping: Route = {
    handle: ping,
    method: Method.Get,
    middlewares: [],
    path: '/ping',
};

export default Ping;
