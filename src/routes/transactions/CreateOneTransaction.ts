import { Request, Response } from 'express';
import Joi from '@hapi/joi';
import { Route, Method } from '@mlaopane/easy-router';
import BadRequestHttpError from '@app/errors/BadRequestHttpError';
import InstalmentFactory from '@app/services/InstalmentFactory';
import InstalmentRepository from '@app/services/InstalmentRepository';
import TransactionFactory from '@app/services/TransactionFactory';
import TransactionRepository from '@app/services/TransactionRepository';
import payNextTransactionInstalment from '@app/services/Payment/payNextTransactionInstalment';
import {
    WithInstalments,
    PersistedTransaction,
} from '@app/models/Transaction';

const schema = Joi.object({
    customerId: Joi.number().required(),
    amount: Joi.number().required(),
    split: Joi.number().required(),
    storeName: Joi.string().max(50).required(),
}).required();

async function validateRequest(request: Request) {
    try {
        await schema.validateAsync(request.body);
    } catch (error) {
        throw new BadRequestHttpError({ errors: error.details });
    }
}

async function createOneTransaction(request: Request, response: Response) {
    await validateRequest(request);

    const transaction = TransactionFactory.createTransactionFromInput(
        request.body
    );

    const persistedTransaction = await TransactionRepository.insertOneTransaction(
        transaction
    );

    const instalments = InstalmentFactory.createInstalmentsFromTransaction(
        persistedTransaction
    );

    await InstalmentRepository.insertManyInstalments(instalments);

    await payNextTransactionInstalment(persistedTransaction);

    const transactionWithInstalments: WithInstalments<PersistedTransaction> = {
        ...persistedTransaction,
        instalments: await InstalmentRepository.getManyInstalmentsByTransactionId(
            persistedTransaction.id
        ),
    };

    response.status(201).json(transactionWithInstalments);
}

const CreateOneTransaction: Route = {
    handle: createOneTransaction,
    method: Method.Post,
    middlewares: [],
    path: '/transactions',
};

export default CreateOneTransaction;
