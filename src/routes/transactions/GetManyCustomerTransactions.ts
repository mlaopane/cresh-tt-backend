import { Request, Response } from 'express';
import { Route, Method } from '@mlaopane/easy-router';
import TransactionRepository from '@app/services/TransactionRepository';
import CustomerRepository from '@app/services/CustomerRepository';

async function getManyCustomerTransactions(
    request: Request,
    response: Response
) {
    const customerId = parseInt(request.params.id, 10);
    await CustomerRepository.getOneCustomerById(customerId);
    const transactions = await TransactionRepository.getManyTransactionsByCustomerId(
        customerId
    );

    response.status(200).json(transactions);
}

const GetManyCustomerTransactions: Route = {
    handle: getManyCustomerTransactions,
    method: Method.Get,
    middlewares: [],
    path: '/customers/:id(\\d+)/transactions',
};

export default GetManyCustomerTransactions;
