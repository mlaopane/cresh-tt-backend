import { Request, Response } from 'express';
import { Route, Method } from '@mlaopane/easy-router';
import InstalmentRepository from '@app/services/InstalmentRepository';
import TransactionRepository from '@app/services/TransactionRepository';

async function getManyTransactionInstalments(
    request: Request,
    response: Response
) {
    const transactionId = parseInt(request.params.id, 10);
    await TransactionRepository.getOneTransactionById(transactionId);
    const instalments = await InstalmentRepository.getManyInstalmentsByTransactionId(
        transactionId
    );

    response.status(200).json(instalments);
}

const GetManyTransactionInstalments: Route = {
    handle: getManyTransactionInstalments,
    method: Method.Get,
    middlewares: [],
    path: '/transactions/:id(\\d+)/instalments',
};

export default GetManyTransactionInstalments;
