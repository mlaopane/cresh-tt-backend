import GetManyTransactionInstalments from './GetManyTransactionInstalments';
import TriggerPayForNextTransactionInstalment from './TriggerPayForNextTransactionInstalment';

export default {
    GetManyTransactionInstalments,
    TriggerPayForNextTransactionInstalment,
};
