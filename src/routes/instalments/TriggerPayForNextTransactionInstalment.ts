import { Request, Response } from 'express';
import { Route, Method } from '@mlaopane/easy-router';
import TransactionRepository from '@app/services/TransactionRepository';
import payNextTransactionInstalment from '@app/services/Payment/payNextTransactionInstalment';

async function triggerPayForNextTransactionInstalment(
    request: Request,
    response: Response
) {
    const transactionId = parseInt(request.params.id, 10);
    const transaction = await TransactionRepository.getOneTransactionById(
        transactionId
    );

    await payNextTransactionInstalment(transaction);

    response.status(204).end();
}

const TriggerPayForNextTransactionInstalment: Route = {
    handle: triggerPayForNextTransactionInstalment,
    method: Method.Put,
    middlewares: [],
    path: '/transactions/:id(\\d+)/instalments',
};

export default TriggerPayForNextTransactionInstalment;
