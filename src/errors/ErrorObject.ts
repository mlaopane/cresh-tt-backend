export interface ErrorObject {
    detail: string;
    source: string;
}
