import { PersistedTransaction } from '@app/models/Transaction';

export default class TransactionAlreadyCompleted extends Error {
    constructor(transactionId: PersistedTransaction['id']) {
        super(`The transaction ${transactionId} is already completed`);
    }
}
