import HttpError from '@app/errors/HttpError';
import { ErrorObject } from '@app/errors/ErrorObject';

interface ConstructorProps {
    errors: ErrorObject[];
}

export default class BadRequestHttpError {
    constructor({ errors = [] }: ConstructorProps) {
        return new HttpError({
            statusCode: 400,
            message: 'Bad request',
            errors,
        });
    }
}
