import { ErrorObject } from '@app/errors/ErrorObject';

interface ConstructorProps {
    statusCode: number;
    errors: ErrorObject[];
    message: string;
}

export default class HttpError {
    public readonly errors: ErrorObject[];
    public readonly message: string;
    public readonly statusCode: number;

    constructor({ statusCode, errors, message }: ConstructorProps) {
        this.message = message;
        this.statusCode = statusCode;
        this.errors = errors;
    }
}
