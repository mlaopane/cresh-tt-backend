import HttpError from '@app/errors/HttpError';
import { ErrorObject } from '@app/errors/ErrorObject';

interface ConstructorProps extends ErrorObject {}

export default class NotFoundHttpError {
    constructor({ source, detail }: ConstructorProps) {
        return new HttpError({
            statusCode: 404,
            message: 'Resource not found',
            errors: [
                {
                    source,
                    detail,
                },
            ],
        });
    }
}
