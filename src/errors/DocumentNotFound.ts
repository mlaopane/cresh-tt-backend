interface ConstructorProps {
    id: number;
    message: string;
}
export default class DocumentNotFound extends Error {
    public readonly code = 404;
    public readonly id: number;
    public readonly message: string;

    constructor({ id, message }: ConstructorProps) {
        super(message);
        this.id = id;
        this.message = message;
    }
}
