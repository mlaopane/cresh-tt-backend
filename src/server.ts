import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import router from '@app/router';
import handleError from '@app/middlewares/handleError';

const server = express();

server.use(helmet());
server.use(cors());
server.use(express.json());
server.use(router.expressRouter);
server.use(handleError);

export default server;
