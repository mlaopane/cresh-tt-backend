import { createEasyRouter } from '@mlaopane/easy-router';
import { Ping, Customers, Instalments, Transactions } from '@app/routes';
import handleDocumentNotFoundError from '@app/middlewares/handleDocumentNotFoundError';
import handleHttpError from '@app/middlewares/handleHttpError';
import handleValidationError from '@app/middlewares/handleValidationError';

const router = createEasyRouter();

router.plugRoutesPool(Ping);
router.plugRoutesPool(Customers);
router.plugRoutesPool(Instalments);
router.plugRoutesPool(Transactions);
router.expressRouter.use(handleValidationError);
router.expressRouter.use(handleDocumentNotFoundError);
router.expressRouter.use(handleHttpError);

export default router;
