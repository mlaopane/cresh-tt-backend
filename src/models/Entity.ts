import { PrimaryKey } from '@app/types';

export type Entity<Model> = Model & { id: PrimaryKey };
