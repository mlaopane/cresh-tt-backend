import { Instalment } from './Instalment';
import { Entity } from './Entity';

export interface BaseTransaction {
    customerId: number; // MANY Transactions for ONE Customer
    amount: number; // in cents
    storeName: string;
    split: number;
}
export interface Transaction extends BaseTransaction {
    isCompleted: boolean;
    createdDate: Date;
}
export type PersistedTransaction = Entity<Transaction>;
export type WithInstalments<T> = T & { instalments: Instalment[] };
