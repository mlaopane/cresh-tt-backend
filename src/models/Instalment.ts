import { Entity } from './Entity';

export interface BaseInstalment {
    transactionId: number; // MANY Instalments for ONE Transaction
    amount: number; // in cents
    paidDate: Date | null;
    plannedDate: Date;
}
export interface Instalment extends BaseInstalment {
    isPaid: boolean;
}
export type PersistedInstalment = Entity<Instalment>;
