import { Transaction } from './Transaction';
import { Entity } from './Entity';

interface BaseCustomer {
    name: string;
}
export interface Customer extends BaseCustomer {
    createdDate: Date;
}
export type PersistedCustomer = Entity<Customer>;
export type WithTransactions<T> = T & { transactions: Transaction[] };
