import { ErrorRequestHandler } from 'express';
import DocumentNotFound from '@app/errors/DocumentNotFound';
import NotFoundHttpError from '@app/errors/NotFoundHttpError';

const handleDocumentNotFoundError: ErrorRequestHandler = (
    error,
    request,
    response,
    next
) => {
    if (error instanceof DocumentNotFound) {
        throw new NotFoundHttpError({
            source: request.url,
            detail: `ID not found: ${error.id}`,
        });
    } else {
        next(error);
    }
};

export default handleDocumentNotFoundError;
