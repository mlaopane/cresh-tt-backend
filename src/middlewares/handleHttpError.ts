import { ErrorRequestHandler } from 'express';
import HttpError from '@app/errors/HttpError';

const handleHttpError: ErrorRequestHandler = (
    error,
    request,
    response,
    next,
) => {
    if (error instanceof HttpError) {
        response.status(error.statusCode).json({
            message: error.message,
            errors: error.errors,
        });
    } else {
        next(error);
    }
};

export default handleHttpError;
