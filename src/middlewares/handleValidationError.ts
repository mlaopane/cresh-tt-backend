import { ErrorRequestHandler } from 'express';
import BadRequestHttpError from '@app/errors/BadRequestHttpError';

const handleValidationError: ErrorRequestHandler = (
    error,
    request,
    response,
    next
) => {
    if (error.isJoi) {
        throw new BadRequestHttpError({ errors: error.details });
    }

    next(error);
};

export default handleValidationError;
