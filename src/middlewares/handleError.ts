import { ErrorRequestHandler } from 'express';

function resolveResponseFromError(error: any) {
    const statusCode = error.statusCode || 500;
    const message = error.message || 'An unexpected error occured';

    return {
        statusCode,
        body: { message },
    };
}

const handleError: ErrorRequestHandler = (error, request, response, next) => {
    const { statusCode, body } = resolveResponseFromError(error);
    response.status(statusCode).json(body);
};

export default handleError;
