import { Entity } from '@app/models/Entity';

export interface DataSource<T> {
    findAll: () => Promise<Entity<T>[]>;
    findBy: (criteria: any) => Promise<Entity<T>[]>;
    findOneBy: (criteria: any) => Promise<Entity<T> | null>;
    insertOne: (entityInput: T) => Promise<Entity<T>>;
    insertMany: (entityInput: T[]) => Promise<Entity<T>[]>;
    updateOne: (entityInput: Entity<T>) => Promise<void>;
}

export type PrimaryKey = number;
