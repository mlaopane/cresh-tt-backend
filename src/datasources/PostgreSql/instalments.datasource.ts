import pool from '@app/database';
import { DataSource } from '@app/types';
import { PersistedInstalment, Instalment } from '@app/models/Instalment';
import { PersistedTransaction } from '@app/models/Transaction';

const InstalmentsDataSource: DataSource<Instalment> = {
    async findAll() {
        const client = await pool.connect();
        const result = await client.query<PersistedInstalment>(
            `SELECT * FROM instalments`
        );
        client.release();
        const instalments = result.rows;

        return instalments;
    },
    async findBy(criteria: { transactionId: PersistedTransaction['id'] }) {
        const client = await pool.connect();
        const result = await client.query<PersistedInstalment>(
            `SELECT * FROM instalments WHERE "transactionId" = $1`,
            [criteria.transactionId]
        );
        client.release();
        const instalments = result.rows;

        return instalments;
    },
    async findOneBy() {
        return null;
    },
    async insertOne(newInstalment: Instalment) {
        const client = await pool.connect();
        const result = await client.query<PersistedInstalment>(
            `INSERT INTO instalments (amount, "transactionId", "isPaid", "paidDate", "plannedDate")
            VALUES ($1, $2, $3, $4, $5) RETURNING id;`,
            [
                newInstalment.amount,
                newInstalment.transactionId,
                newInstalment.isPaid,
                newInstalment.paidDate,
                newInstalment.plannedDate,
            ]
        );
        client.release();
        const [{ id }] = result.rows;

        return { ...newInstalment, id };
    },
    async insertMany(newInstalments: Instalment[]) {
        const createdInstalments: PersistedInstalment[] = await Promise.all(
            newInstalments.map((newInstalment) =>
                this.insertOne(newInstalment)
            )
        );

        return createdInstalments;
    },
    async updateOne(instalment: PersistedInstalment) {
        const client = await pool.connect();
        await client.query<PersistedInstalment>(
            `UPDATE instalments
            SET
                amount = $2,
                "isPaid" = $3,
                "paidDate" = $4,
                "plannedDate" = $5
            WHERE id = $1;`,
            [
                instalment.id,
                instalment.amount,
                instalment.isPaid,
                instalment.paidDate,
                instalment.plannedDate,
            ]
        );
        client.release();
    },
};

export default InstalmentsDataSource;
