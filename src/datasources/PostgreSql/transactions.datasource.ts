import { Transaction, PersistedTransaction } from '@app/models/Transaction';
import { DataSource } from '@app/types';
import pool from '@app/database';

const TransactionsDataSource: DataSource<Transaction> = {
    async findAll() {
        const client = await pool.connect();
        const result = await client.query<PersistedTransaction>(
            `SELECT * FROM transactions`
        );
        client.release();
        const transactions = result.rows;

        return transactions;
    },

    async findBy(criteria: {
        customerId: PersistedTransaction['customerId'];
    }) {
        const client = await pool.connect();
        const result = await client.query<PersistedTransaction>(
            `SELECT * FROM transactions WHERE "customerId" = $1`,
            [criteria.customerId]
        );
        client.release();
        const transactions = result.rows;

        return transactions;
    },

    async findOneBy(criteria: { id: PersistedTransaction['id'] }) {
        const client = await pool.connect();
        const result = await client.query<PersistedTransaction>(
            `SELECT * FROM transactions WHERE id = $1`,
            [criteria.id]
        );
        client.release();
        const [transaction] = result.rows;

        return transaction || null;
    },

    async insertOne(newTransaction: Transaction) {
        const client = await pool.connect();
        const result = await client.query<PersistedTransaction>(
            `INSERT INTO transactions (amount, "createdDate", "customerId", "isCompleted", split, "storeName")
            VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;`,
            [
                newTransaction.amount,
                newTransaction.createdDate,
                newTransaction.customerId,
                newTransaction.isCompleted,
                newTransaction.split,
                newTransaction.storeName,
            ]
        );
        client.release();
        const [{ id }] = result.rows;

        return { ...newTransaction, id };
    },

    async insertMany() {
        return [];
    },

    async updateOne(transaction: PersistedTransaction) {
        const client = await pool.connect();
        await client.query<PersistedTransaction>(
            `UPDATE transactions
            SET
                amount = $2,
                "createdDate" = $3,
                "customerId" = $4,
                "isCompleted" = $5,
                split = $6,
                "storeName" = $7
            WHERE id = $1;`,
            [
                transaction.id,
                transaction.amount,
                transaction.createdDate,
                transaction.customerId,
                transaction.isCompleted,
                transaction.split,
                transaction.storeName,
            ]
        );
        client.release();
    },
};

export default TransactionsDataSource;
