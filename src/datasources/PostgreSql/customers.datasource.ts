import { Customer, PersistedCustomer } from '@app/models/Customer';
import { DataSource } from '@app/types';
import pool from '@app/database';

const CustomersDataSource: DataSource<Customer> = {
    async findAll() {
        const client = await pool.connect();
        const result = await client.query<PersistedCustomer>(
            `SELECT * FROM customers`
        );
        client.release();

        const customers = result.rows;

        return customers;
    },
    async findBy() {
        return [];
    },
    async findOneBy(criteria: { id: PersistedCustomer['id'] }) {
        const client = await pool.connect();
        const result = await client.query<PersistedCustomer>(
            `SELECT * FROM customers WHERE id = $1`,
            [criteria.id]
        );
        client.release();

        const [customer] = result.rows;

        return customer || null;
    },
    async insertOne(newCustomer) {
        const client = await pool.connect();
        const result = await client.query<PersistedCustomer>(
            `INSERT INTO customers (name, "createdDate") VALUES ($1, $2) RETURNING id`,
            [newCustomer.name, newCustomer.createdDate]
        );
        client.release();
        const [{ id }] = result.rows;

        return { ...newCustomer, id };
    },
    async insertMany() {
        return [];
    },
    async updateOne() {},
};

export default CustomersDataSource;
