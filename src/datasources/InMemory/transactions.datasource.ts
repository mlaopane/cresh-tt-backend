import { Transaction, PersistedTransaction } from '@app/models/Transaction';
import { DataSource, PrimaryKey } from '@app/types';

const transactions: PersistedTransaction[] = [
    {
        id: 1,
        customerId: 1,
        createdDate: new Date('2020-04-11T00:00:00Z'),
        amount: 50000,
        isCompleted: false,
        split: 3,
        storeName: 'Castorama',
    },
    {
        id: 2,
        customerId: 1,
        createdDate: new Date('2020-04-11T00:00:00Z'),
        amount: 20000,
        isCompleted: false,
        split: 2,
        storeName: 'Decathlon',
    },
    {
        id: 3,
        customerId: 2,
        createdDate: new Date('2020-03-30T00:00:00Z'),
        amount: 45000,
        isCompleted: false,
        split: 3,
        storeName: 'Conforama',
    },
];

function getGreaterTransactionId(): PrimaryKey {
    return transactions.reduce((max, transaction) => {
        if (transaction.id > max) {
            return transaction.id;
        }

        return max;
    }, 0);
}

function getNextTransactionId(): PrimaryKey {
    return getGreaterTransactionId() + 1;
}

const TransactionsDataSource: DataSource<Transaction> = {
    async findAll() {
        return transactions;
    },
    async findBy(criteria: { customerId: Transaction['customerId'] }) {
        return transactions.filter(
            (transaction) => transaction.customerId === criteria.customerId
        );
    },
    async findOneBy(criteria: { id: PersistedTransaction['id'] }) {
        return (
            transactions.find(
                (transaction) => transaction.id === criteria.id
            ) || null
        );
    },
    async insertOne(newTransaction) {
        const createdTransaction: PersistedTransaction = {
            ...newTransaction,
            id: getNextTransactionId(),
        };

        transactions.push(createdTransaction);

        return createdTransaction;
    },
    async insertMany() {
        return [];
    },
    async updateOne(newTransaction: PersistedTransaction) {
        const transactionIndex = transactions.findIndex(
            (transaction) => transaction.id === newTransaction.id
        );

        if (transactionIndex === -1) {
            return;
        }

        transactions[transactionIndex] = {
            ...transactions[transactionIndex],
            ...newTransaction,
        };
    },
};

export default TransactionsDataSource;
