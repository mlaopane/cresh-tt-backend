import { DataSource, PrimaryKey } from '@app/types';
import { PersistedInstalment, Instalment } from '@app/models/Instalment';
import { PersistedTransaction } from '@app/models/Transaction';

const instalments: PersistedInstalment[] = [];

function getGreaterInstalmentId(): PrimaryKey {
    return instalments.reduce((max, instalment) => {
        if (instalment.id > max) {
            return instalment.id;
        }

        return max;
    }, 0);
}

function getNextInstalmentId(): PrimaryKey {
    return getGreaterInstalmentId() + 1;
}

const InstalmentsDataSource: DataSource<Instalment> = {
    async findAll() {
        return instalments;
    },
    async findBy(criteria: { transactionId: PersistedTransaction['id'] }) {
        return instalments.filter(
            (instalment) => instalment.transactionId === criteria.transactionId
        );
    },
    async findOneBy() {
        return null;
    },
    async insertOne(newInstalment) {
        const createdInstalment: PersistedInstalment = {
            ...newInstalment,
            id: getNextInstalmentId(),
        };

        instalments.push(createdInstalment);

        return createdInstalment;
    },
    async insertMany(newInstalments: Instalment[]) {
        const createdInstalments: PersistedInstalment[] = newInstalments.map(
            (newInstalment, index) => ({
                ...newInstalment,
                id: getNextInstalmentId() + index,
            })
        );

        instalments.push(...createdInstalments);

        return createdInstalments;
    },
    async updateOne(newInstalment: PersistedInstalment) {
        const dbInstalmentIndex = instalments.findIndex(
            (instalment) => instalment.id === newInstalment.id
        );

        if (dbInstalmentIndex === -1) {
            return;
        }

        instalments[dbInstalmentIndex] = {
            ...instalments[dbInstalmentIndex],
            ...newInstalment,
        };
    },
};

export default InstalmentsDataSource;
