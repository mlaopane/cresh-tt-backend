import { Customer, PersistedCustomer } from '@app/models/Customer';
import { DataSource, PrimaryKey } from '@app/types';

const customers: PersistedCustomer[] = [
    {
        id: 1,
        createdDate: new Date('2020-04-11T00:00:00Z'),
        name: 'Jon DO',
    },
    {
        id: 2,
        createdDate: new Date('2020-04-11T00:00:00Z'),
        name: 'Jane DO',
    },
];

function getGreaterCustomerId(): PrimaryKey {
    return customers.reduce((max, customer) => {
        if (customer.id > max) {
            return customer.id;
        }

        return max;
    }, 0);
}

function getNextCustomerId(): PrimaryKey {
    return getGreaterCustomerId() + 1;
}

const CustomersDataSource: DataSource<Customer> = {
    async findAll() {
        return customers;
    },
    async findBy() {
        return [];
    },
    async findOneBy(criteria: { id: PersistedCustomer['id'] }) {
        return (
            customers.find((customer) => customer.id === criteria.id) || null
        );
    },
    async insertOne(newCustomer) {
        const createdCustomer: PersistedCustomer = {
            ...newCustomer,
            id: getNextCustomerId(),
        };

        customers.push(createdCustomer);

        return createdCustomer;
    },
    async insertMany() {
        return [];
    },
    async updateOne() {},
};

export default CustomersDataSource;
