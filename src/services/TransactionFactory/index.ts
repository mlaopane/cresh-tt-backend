import createTransactionFromInput from './createTransactionFromInput';

const TransactionFactory = { createTransactionFromInput };

export default TransactionFactory;
