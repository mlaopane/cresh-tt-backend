import { BaseTransaction, Transaction } from '@app/models/Transaction';

export default function createTransactionFromInput(
    transactionInput: BaseTransaction
): Transaction {
    return {
        ...transactionInput,
        isCompleted: false,
        createdDate: new Date(),
    };
}
