import insertOneCustomer from './insertOneCustomer';
import getManyCustomers from './getManyCustomers';
import getOneCustomerById from './getOneCustomerById';

const CustomerRepository = {
    insertOneCustomer,
    getManyCustomers,
    getOneCustomerById,
};

export default CustomerRepository;
