import { Customer } from '@app/models/Customer';
import CustomersDataSource from '@app/datasources/PostgreSql/customers.datasource';

export default async function insertOneCustomer(newCustomer: Customer) {
    return await CustomersDataSource.insertOne(newCustomer);
}
