import { Customer } from '@app/models/Customer';
import CustomersDataSource from '@app/datasources/PostgreSql/customers.datasource';
import DocumentNotFound from '@app/errors/DocumentNotFound';

export default async function getOneCustomerById(
    id: number
): Promise<Customer> {
    const customer = await CustomersDataSource.findOneBy({ id });

    if (null === customer) {
        throw new DocumentNotFound({
            id,
            message: `This customer does not exist`,
        });
    }

    return customer;
}
