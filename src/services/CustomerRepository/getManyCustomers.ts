import { Customer } from '@app/models/Customer';
import CustomersDataSource from '@app/datasources/PostgreSql/customers.datasource';

export default async function getManyCustomers(): Promise<Customer[]> {
    return CustomersDataSource.findAll();
}
