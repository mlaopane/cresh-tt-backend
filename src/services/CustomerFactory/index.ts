import createCustomerFromInput from './createCustomerFromInput';

const CustomerFactory = { createCustomerFromInput };

export default CustomerFactory;
