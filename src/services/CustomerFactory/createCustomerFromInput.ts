import { Customer } from '@app/models/Customer';

export default function createCustomerFromInput(customerInput): Customer {
    return {
        ...customerInput,
        createdDate: new Date(),
    };
}
