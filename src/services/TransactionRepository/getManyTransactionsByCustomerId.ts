import TransactionsDataSource from '@app/datasources/PostgreSql/transactions.datasource';
import { Transaction } from '@app/models/Transaction';

export default async function getManyTransactionsByCustomerId(
    customerId: Transaction['customerId']
): Promise<Transaction[]> {
    return TransactionsDataSource.findBy({ customerId });
}
