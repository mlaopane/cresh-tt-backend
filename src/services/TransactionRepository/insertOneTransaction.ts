import transactions from '@app/datasources/PostgreSql/transactions.datasource';
import { Transaction } from '@app/models/Transaction';

export default async function insertOneTransaction(
    newTransaction: Transaction
) {
    return await transactions.insertOne(newTransaction);
}
