import InstalmentsDataSource from '@app/datasources/PostgreSql/instalments.datasource';
import TransactionsDataSource from '@app/datasources/PostgreSql/transactions.datasource';
import { PersistedTransaction } from '@app/models/Transaction';

export default async function completeTransactionIfAllInstalmentsHaveBeenPaid(
    transaction: PersistedTransaction
) {
    const transactionsInstalments = await InstalmentsDataSource.findBy({
        transactionId: transaction.id,
    });

    const isCompleted = transactionsInstalments.reduce(
        (allInstalmentsArePaid, instalment) => {
            return allInstalmentsArePaid && instalment.isPaid;
        },
        true
    );

    TransactionsDataSource.updateOne({ ...transaction, isCompleted });
}
