import completeTransactionIfAllInstalmentsHaveBeenPaid from './completeTransactionIfAllInstalmentsHaveBeenPaid';
import getManyTransactionsByCustomerId from './getManyTransactionsByCustomerId';
import getOneTransactionById from './getOneTransactionById';
import insertOneTransaction from './insertOneTransaction';

const TransactionRepository = {
    completeTransactionIfAllInstalmentsHaveBeenPaid,
    getManyTransactionsByCustomerId,
    getOneTransactionById,
    insertOneTransaction,
};

export default TransactionRepository;
