import { PersistedTransaction } from '@app/models/Transaction';
import TransactionsDataSource from '@app/datasources/PostgreSql/transactions.datasource';
import DocumentNotFound from '@app/errors/DocumentNotFound';

export default async function getOneTransactionById(
    id: number
): Promise<PersistedTransaction> {
    const transaction = await TransactionsDataSource.findOneBy({ id });

    if (null === transaction) {
        throw new DocumentNotFound({
            id,
            message: `This transaction does not exist`,
        });
    }

    return transaction;
}
