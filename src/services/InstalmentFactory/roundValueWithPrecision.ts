export default function roundValueWithPrecision(
    value: number,
    precision: number
) {
    const powerOfTen = Math.pow(10, precision);
    return Math.round(powerOfTen * value) / powerOfTen;
}
