import floorValueWithPrecision from './floorValueWithPrecision';

export default function floorAmount(amount: number) {
    return floorValueWithPrecision(amount, 0);
}
