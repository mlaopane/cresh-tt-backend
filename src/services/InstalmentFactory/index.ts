import createInstalmentsFromTransaction from './createInstalmentsFromTransaction';

const InstalmentFactory = { createInstalmentsFromTransaction };

export default InstalmentFactory;
