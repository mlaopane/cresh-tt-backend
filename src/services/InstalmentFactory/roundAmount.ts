import roundValueWithPrecision from './roundValueWithPrecision';

export default function roundAmount(amount: number) {
    return roundValueWithPrecision(amount, 0);
}
