export default function floorValueWithPrecision(
    value: number,
    precision: number
) {
    const powerOfTen = Math.pow(10, precision);
    return Math.floor(powerOfTen * value) / powerOfTen;
}
