import moment from 'moment';
import { Instalment } from '@app/models/Instalment';
import {
    PersistedTransaction,
    BaseTransaction,
} from '@app/models/Transaction';
import floorAmount from '@app/services/InstalmentFactory/floorAmount';
import roundAmount from '@app/services/InstalmentFactory/roundAmount';

function calculateInstalmentAmountFromTransaction(
    transaction: BaseTransaction
): number {
    return floorAmount(transaction.amount / transaction.split);
}

function calculateFirstInstalmentAmountFromTransaction(
    transaction: BaseTransaction
): number {
    const instalmentAmount = calculateInstalmentAmountFromTransaction(
        transaction
    );
    const totalInstalmentsAmount = instalmentAmount * transaction.split;
    const remainderAmount = transaction.amount - totalInstalmentsAmount;
    return roundAmount(instalmentAmount + remainderAmount);
}

function calculateNextPlannedDate(previousDate: Date) {
    return moment(previousDate).add(1, 'month').toDate();
}

export default function createInstalmentsFromTransaction(
    transaction: PersistedTransaction
): Instalment[] {
    const splits = Array.from({ length: transaction.split });

    return splits.reduce((stack: Instalment[], _, index) => {
        if (index === 0) {
            return [
                {
                    amount: calculateFirstInstalmentAmountFromTransaction(
                        transaction
                    ),
                    paidDate: null,
                    plannedDate: calculateNextPlannedDate(new Date()),
                    isPaid: false,
                    transactionId: transaction.id,
                },
            ];
        }

        return [
            ...stack,
            {
                amount: calculateInstalmentAmountFromTransaction(transaction),
                paidDate: null,
                plannedDate: calculateNextPlannedDate(
                    stack[index - 1].plannedDate
                ),
                isPaid: false,
                transactionId: transaction.id,
            },
        ];
    }, []);
}
