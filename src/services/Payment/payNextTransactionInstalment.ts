import { PersistedTransaction } from '@app/models/Transaction';
import InstalmentRepository from '@app/services/InstalmentRepository';
import TransactionRepository from '@app/services/TransactionRepository';
import { PersistedInstalment } from '@app/models/Instalment';
import TransactionAlreadyCompleted from '@app/errors/TransactionAlreadyCompleted';
import HttpError from '@app/errors/HttpError';

export default async function payNextTransactionInstalment(
    transaction: PersistedTransaction
) {
    try {
        const instalment: PersistedInstalment = await InstalmentRepository.getNextUnpaidInstalmentByTransactionId(
            transaction.id
        );
        instalment.paidDate = new Date();
        instalment.isPaid = true;
        await InstalmentRepository.updateOneInstalment(instalment);
        await TransactionRepository.completeTransactionIfAllInstalmentsHaveBeenPaid(
            transaction
        );
    } catch (error) {
        if (error instanceof TransactionAlreadyCompleted) {
            throw new HttpError({
                statusCode: 409,
                message: error.message,
                errors: [],
            });
        }
        throw error;
    }
}
