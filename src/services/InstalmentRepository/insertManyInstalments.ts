import InstalmentsDataSource from '@app/datasources/PostgreSql/instalments.datasource';
import { Instalment } from '@app/models/Instalment';

export default async function insertManyInstalments(
    instalments: Instalment[]
) {
    return await InstalmentsDataSource.insertMany(instalments);
}
