import getManyInstalmentsByTransactionId from './getManyInstalmentsByTransactionId';
import getNextUnpaidInstalmentByTransactionId from './getNextUnpaidInstalmentByTransactionId';
import insertManyInstalments from './insertManyInstalments';
import updateOneInstalment from './updateOneInstalment';

const InstalmentRepository = {
    getManyInstalmentsByTransactionId,
    getNextUnpaidInstalmentByTransactionId,
    insertManyInstalments,
    updateOneInstalment,
};

export default InstalmentRepository;
