import InstalmentsDataSource from '@app/datasources/PostgreSql/instalments.datasource';
import { PersistedInstalment } from '@app/models/Instalment';

export default async function updateOneInstalment(
    instalment: PersistedInstalment
) {
    return await InstalmentsDataSource.updateOne(instalment);
}
