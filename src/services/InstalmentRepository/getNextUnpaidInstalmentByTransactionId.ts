import InstalmentsDataSource from '@app/datasources/PostgreSql/instalments.datasource';
import { PersistedTransaction } from '@app/models/Transaction';
import { PersistedInstalment } from '@app/models/Instalment';
import TransactionAlreadyCompleted from '@app/errors/TransactionAlreadyCompleted';

export default async function getNextUnpaidInstalmentByTransactionId(
    transactionId: PersistedTransaction['id']
): Promise<PersistedInstalment> {
    const transactionInstalments = (
        await InstalmentsDataSource.findBy({
            transactionId,
        })
    ).sort((current, next) =>
        current.plannedDate < next.plannedDate ? -1 : 1
    );

    const nextUnpaidInstalment = transactionInstalments.find(
        (transactionInstalment) => transactionInstalment.isPaid === false
    );

    if (undefined === nextUnpaidInstalment) {
        throw new TransactionAlreadyCompleted(transactionId);
    }

    return nextUnpaidInstalment;
}
