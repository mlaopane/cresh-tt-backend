import InstalmentsDataSource from '@app/datasources/PostgreSql/instalments.datasource';
import { PersistedTransaction } from '@app/models/Transaction';
import { PersistedInstalment } from '@app/models/Instalment';

export default async function getManyInstalmentsByTransactionId(
    transactionId: PersistedTransaction['id']
): Promise<PersistedInstalment[]> {
    return InstalmentsDataSource.findBy({ transactionId });
}
