import server from './server';

const SERVER_PORT = process.env.SERVER_PORT;

// Start the server
server.listen(SERVER_PORT, () => {
    console.log(`Server running @ http://localhost:${SERVER_PORT}`);
});
